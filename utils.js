//定数設定
var _dataType="xml";
var _timeout = 1000
var _regstr = /,$/

var _errorFunc = function(){
    alert("ロード失敗")
}
var _showXml = function(xml){
    $(xml).find("template").each(function(){
        if ($(this).children("template").text() != ""){
            console.log($(this).children("name").text())
        }
    })
}

//メイン関数
//実装できたモジュールをどんどん追加していく。
var _success = function(xml){
    _setTemplateSheet(xml)
    _setApplicationSheet(xml)
    _setItemSheet(xml)
}
var _successHostXml = function(xml){
    _setHostSheet(xml)
    //ホストシートにデータを出力するための関数
}

var _setHostSheet = function(hostXml){
    var hostSheet = [
        //hostName
        //screenName
        //group
        //ZAIF_IP
        //IFZA_DNS
        //IFZA_ConnectType
        //IFZA_PORT
        //IFSNMP_IP
        //IFSNMP_DNS
        //IFSNMP_ConnectType
        //IFSNMP_PORT
        //IFJMX_IP
        //IFJMX_DNS
        //IFJMX_ConnectTyep
        //IFJMX_PORT
        //IFIPMI_IP
        //IFIPMI_DNS
        //IFIPMI_ConnectType
        //IFIPMI_PORT
        //description
        //proxy
        //status
        //templateLink
        //IPMI_authtype
        //IPMI_priviledge
        //IPMI_username
        //IPMI_password
        //hostInventory{} (未実装)
        //toHostConnect{}
        //fromHostConnect{}
        //note
    ]

    var get_templateLink = function(elem){
        var outstr = ""
        elem.find("template").each(function(){
            outstr += $(this).children("name").text() + " "
        })
        outstr = outstr.replace(/\s$/g,"")
        return outstr
    }

    var get_interface = function(elem, interface){
        var intf = interface
        var get_useip = function(useip){
            switch(useip){
                case "0": return "DNS"
                case "1": return "IPアドレス"
            }
        }
        elem.find("interface").each(function(){
            switch($(this).children("type").text()){
                case "1":
                    //ZAの処理
                    intf.ZA = {
                        ip:$(this).children("ip").text(),
                        dns:$(this).children("dns").text(),
                        useip:$(this).children("useip").text(),
                        port:$(this).children("port").text()
                    }
                    intf.ZA.useip = get_useip(intf.ZA.useip)
                    break;
                case "2":
                    //SNMP
                    intf.SNMP = {
                        ip:$(this).children("ip").text(),
                        dns:$(this).children("dns").text(),
                        useip:$(this).children("useip").text(),
                        port:$(this).children("port").text()
                    }
                    intf.SNMP.useip = get_useip(intf.SNMP.useip)
                    break;
                case "3":
                    //IPMI
                    intf.IPMI = {
                        ip:$(this).children("ip").text(),
                        dns:$(this).children("dns").text(),
                        useip:$(this).children("useip").text(),
                        port:$(this).children("port").text()
                    }
                    intf.IPMI.useip = get_useip(intf.IPMI.useip)
                    break;
                case "4":
                    //JMX
                    intf.JMX = {
                        ip:$(this).children("ip").text(),
                        dns:$(this).children("dns").text(),
                        useip:$(this).children("useip").text(),
                        port:$(this).children("port").text()
                    }
                    intf.JMX.useip = get_useip(intf.JMX.useip)
                    break;
            }
        })
        return intf
    }
    
    $(hostXml).find("host").each(function(){
        if ($(this).children("name").text() != ""){
            var e = $(this)
            console.log(e)
            var obj = {}
            obj = {
                hostName:e.children("host").text(),
                screenName:e.children("name").text(),
                group:"",   //一旦初期化
                interface:{
                    ZA:{
                        ip:"-", //e.children("").text(),
                        dns:"-",   //e.children("").text(),
                        useip:"-", //e.children("").text(),
                        port:"-" //e.children("").text(),
                    },
                    SNMP:{
                        ip:"-", //e.children("").text(),
                        dns:"-", //e.children("").text(),
                        useip:"-", //e.children("").text(),
                        port:"-" //e.children("").text(),
                    },
                    JMX:{
                        ip:"-", //e.children("").text(),
                        dns:"-", //e.children("").text(),
                        useip:"-", //e.children("").text(),
                        port:"-" //e.children("").text(),
                    },
                    IPMI:{
                        ip:"-", //e.children("").text(),
                        dns:"-",//e.children("").text(),
                        useip:"-", //e.children("").text(),
                        port:"-" //e.children("").text(),
                    }
                },
                description:e.children("description").text(),
                proxy:"", //e.children("").text(),
                status:e.children("status").text(),
                templateLink:"", //一旦初期化
                IPMI_authtype:e.children("ipmi_authtype").text(),
                IPMI_priviledge:e.children("ipmi_privilege").text(),
                IPMI_username:e.children("ipmi_username").text(),
                IPMI_password:e.children("ipmi_password").text(),
                inventory:{
                    type:"",
                    typed:"",
                    name:"",
                    alias:"",
                    os:"",
                    osd:"",
                    ose:"",
                    serialA:"",
                    serialB:"",
                    tag:"",
                    assetTag:"",
                    macAddrA:"",
                    macAddrB:"",
                    hardware:"",
                    hardwared:"",
                    software:"",
                    softwared:"",
                    sotfwareAppA:"",
                    softwareAppB:"",
                    softwareAppC:"",
                    softwareAppD:"",
                    softwareAppE:"",
                    contact:"",
                    place:"",
                    ido:"",
                    keido:"",
                    bikou:"",
                    chassis:"",
                    model:"",
                    vender:"",
                    keiyakuid:"",
                    installerName:"",
                    publishStatus:"",
                    urlA:"",
                    urlB:"",
                    hostNetwork:"",
                    hostSubnet:"",
                    hostRouter:"",
                    OOBIP:"",
                    OOBSUBNET:"",
                    OOBROUTER:"",
                    buyHardware:"",
                    setHardware:"",
                    hardwareExpire:"",
                    addressA:"",
                    addressB:"",
                    addressC:"",
                    city:"",
                    ken:"",
                    country:"",
                    postAddr:"",
                    rack:"",
                    note:"",
                    primaryPOCName:"",
                    primaryPOCMail:"",
                    primaryPOCTelA:"",
                    primaryPOCtelB:"",
                    primaryPOCCell:"",
                    primaryPOCScreenName:"",
                    primaryPOCNote:"",
                    secondaryPOCName:"",
                    secondaryPOCMail:"",
                    secondaryPOCTelA:"",
                    secondaryPOCTelB:"",
                    secondaryPOCCell:"",
                    secondaryPOCNote:""
                }, //(未実装)
                toHostConnect:{
                    cipher:"",
                    PSK:"",
                    crt:""
                },
                fromHostConnect:{
                    cipher:"",
                    PSK:"",
                    crt:""
                },
                note:""  
            }
            obj.templateLink = get_templateLink($(this).children("templates"))
            obj.interface = get_interface($(this).children("interfaces"),obj.interface)
            hostSheet.push(obj)
        }
    })
    var insertStr = ""  //初期化
    hostSheet.forEach(e =>{
        //console.log(e)
        insertStr = `<tr>
            <td>${e.hostName}</td>
            <td>${e.screenName}</td>
            <td>${e.group}</td>
            <td>${e.interface.ZA.ip}</td>
            <td>${e.interface.ZA.dns}</td>
            <td>${e.interface.ZA.useip}</td>
            <td>${e.interface.ZA.port}</td>
            <td>${e.interface.SNMP.ip}</td>
            <td>${e.interface.SNMP.dns}</td>
            <td>${e.interface.SNMP.useip}</td>
            <td>${e.interface.SNMP.port}</td>
            <td>${e.interface.JMX.ip}</td>
            <td>${e.interface.JMX.dns}</td>
            <td>${e.interface.JMX.useip}</td>
            <td>${e.interface.JMX.port}</td>
            <td>${e.interface.IPMI.ip}</td>
            <td>${e.interface.IPMI.dns}</td>
            <td>${e.interface.IPMI.useip}</td>
            <td>${e.interface.IPMI.port}</td>
            <td>${e.description}</td>
            <td>${e.proxy}</td>
            <td>${e.status}</td>
            <td>${e.templateLink}</td>
            <td>${e.IPMI_authtype}</td>
            <td>${e.IPMI_priviledge}</td>
            <td>${e.IPMI_username}</td>
            <td>${e.IPMI_password}</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>${e.toHostConnect.cipher}</td>
            <td>${e.toHostConnect.PSK}</td>
            <td>${e.toHostConnect.crt}</td>
            <td>${e.fromHostConnect.cipher}</td>
            <td>${e.fromHostConnect.PSK}</td>
            <td>${e.fromHostConnect.crt}</td>
            <td>${e.note}</td>
        </tr>`
        $("#host").append(insertStr)
    })

}


//WEBシナリオシートにデータを出力するための関数(未実装)
var _setWebScenarioSheet = function(xml){
    //未実装
}

//トリガーシートにデーターを出力するための関数
/*以下パラメーターは未実装
dependencies : "",  //一旦初期化(未実装)
*/
_setTriggerSheet = function(xml){
    var triggerSheet = [
        //templateName
        //triggerName
        //expression
        //typeStr
        //type //継続して障害
        //description
        //url
        //priorityStr
        //priority
        //status
        //dependencies
        //note
    ]
    var get_typeStr = function(type){
        switch(type){
            case 0: return "-"
            case 1: return "☑"
        }
    }
    var get_priorityStr = function(priority){
        switch(priority){
            case 0: return "未分類"
            case 1: return "情報"
            case 2: return "警告"
            case 3: return "軽度の障害"
            case 4: return "重度の障害"
            case 5: return "致命的な障害"
        }
    }
    var get_templateName = function(expression){
        var pattern = /{.*:/g
        var tn = new String(expression.match(pattern))
        tn = tn.replace("{", "")
        tn = tn.replace(":", "")
        return tn
    }

    //メイン処理
    $(xml).find("triggers").each(function(){
        $(this).find("trigger").each(function(){
            if ($(this).children("name") != ""){
                var obj = {}
                obj = {
                    templateName : "",  //一旦初期化
                    triggerName : $(this).children("name").text(),
                    expression : $(this).children("expression").text(),
                    typeStr : "",       //一旦初期化
                    type : $(this).children("type").text(),
                    description : $(this).children("description").text(),
                    url : $(this).children("url").text(),
                    priorityStr : "",   //一旦初期化
                    priority : $(this).children("priority").text(),
                    status : $(this).children("status").text(),
                    dependencies : "",  //一旦初期化(未実装)
                    note : ""          //一旦初期化
                }
                obj.typeStr = get_typeStr(obj.type)
                obj.priorityStr = get_priorityStr(obj.priority)
                obj.templateName = get_templateName(obj.expression)
                triggerSheet.push(obj)
            }
        })
    })
    var insertStr = ""
    triggerSheet.forEach(e => {
        insertStr = `<tr>
            <td>${e.templateName}</td>
            <td>${e.triggerName}</td>
            <td>${e.expression}</td>
            <td>${e.typeStr}</td>
            <td>${e.type}</td>
            <td>${e.description}</td>
            <td>${e.url}</td>
            <td>${e.priorityStr}</td>
            <td>${e.priority}</td>
            <td>${e.status}</td>
            <td>${e.dependencies}</td>
            <td>${e.note}</td>
            </tr>`
        $("#trigger").append(insertStr)
    })
}

//itemSheetにデータ出力するための関数
/* 下記パラメーターの抽出は未実装(20180424)
params : "", //一旦初期化
refresh_type : 更新カスタマイズ-タイプ
refresh_delay : 更新カスタマイズ-更新間隔
refresh_period : 更新カスタマイズ-期間
inventory : ホストインベントリ
*/
var _setItemSheet = function(xml){
    /* 初期設定と内部メソッド群 */
    var itemSheet = [
        //{
        //  templateName:"",
        //  itemName:"",
        //  type:"",
        //  typeNo:"",
        //  key:"",
        //  snmpoid:"",
        //  formula:"",
        //  snmpcommunity:"",
        //  port:"",
        //  data_type:"",
        //  dataTypeNo:"",
        //  value_type:"",
        //  value_typeNo:"",
        //  unit:""
        //  multiplier:"",
        //  delay:"",
        //  refreshInterval:"",
        //  refreshPeriod:"",
        //  history:"",
        //  trend:"",
        //  save:"",
        //  saveNo:"",
        //  mapping:"",
        //  mappingNo:"",
        //  application:"",
        //  hostInventory:"",
        //  description:"",
        //  state:"",
        //  note:""
        //}
    ]
    var logHeader = "func_setItemSheet"
    var get_typeStr = function(type){
        switch(type){
            case "0": return "Zabbix agent"
            case "1": return "SNMPv1 agent"
            case "2": return "Zabbix trapper"
            case "3": return "simple check"
            case "4": return "SNMPv2 agent"
            case "5": return "Zabbix internal"
            case "6": return "SNMPv3 agent"
            case "7": return "Zabbix agent (active)"
            case "8": return "Zabbix aggregate"
            case "9": return "web item"
            case "10": return "external check"
            case "11": return "database monitor"
            case "12": return "IPMI agent"
            case "13": return "SSH agent"
            case "14": return "TELNET agent"
            case "15": return "calculated"
            case "16": return "JMX agent"
            case "17": return "SNMP trap"
        }
    }
    var get_value_typeStr = function(value_type){
        switch(value_type){
            case "0": return "浮動小数"
            case "1": return "文字列"
            case "2": return "ログ"
            case "3": return "整数"
            case "4": return "テキスト"
        }
    }
    var get_data_typeStr = function(data_type){
        switch(data_type){
            case "0": return "10進数"
            case "1": return "8進数"
            case "2": return "12進数"
            case "3": return "bool型"
        }
    }
    var get_deltaStr = function(delta){
        switch(delta){
            case "0": return "なし"
            case "1": return "speed/sec"
            case "2": return "simple change"
        }
    }
    var get_valuemapStr = function(valuemap){
        switch(valuemap){
            case "0": return "not use"
            case "1": return "Service state"
            case "2": return "Host status"
            case "3": return "Windows service state"
            case "4": return "APC Battery Replacement Status"
            case "5": return "APC Battery Status"
            case "6": return "HP Insight System Status"
            case "7": return "Dell Open Manage System Status"
            case "8": return "SNMP interface status (ifOperStatus)"
            case "9": return "SNMP device status (hrDeviceStatus)"
            case "10": return "Zabbix agent ping status"
            case "11": return "SNMP interface status (ifAdminStatus)"
            case "12": return "VMware VirtualMachinePowerState"
            case "13": return "VMware status"
            case "14": return "Value cache operating mode"
        }
    }

    /* メイン処理 */
    $(xml).find("template").each(function(){
        if ($(this).children("name") != ""){
            var _templateName = $(this).children("name").text()
            var itemsElem = $(this).children("items")
            itemsElem.find('item').each(function(){
                if ($(this).children("name") != ""){
                    var e = $(this)
                    //applicationの整形
                    var _appName = ""
                    $(this).find("application").each(function(){
                        if ($(this).children("name") != ""){
                            _appName += $(this).children("name").text() + " "
                        }
                    })
                    _appName = _appName.replace("/ $/","")

                    //一時的な格納オブジェクト
                    var obj = {} 
                    obj = {
                        templateName : _templateName,
                        itemName : e.children("name").text(),
                        typeStr : "",           //一旦初期化
                        type : e.children("type").text(),
                        key : e.children("key").text(),
                        snmp_oid : e.children("snmp_oid").text(),
                        params : "",            //一旦初期化(未実装)
                        snmp_community : e.children("snmp_community").text(),
                        port : e.children("port").text(),
                        data_typeStr : "",      //一旦初期化
                        data_type : e.children("data_type").text(),
                        value_typeStr: "",      //一旦初期化
                        value_type : e.children("value_type").text(),
                        unit : e.children("unit").text(),
                        formula : e.children("formula").text(),
                        delay : e.children("delay").text(),
                        refresh_type : "",      //一旦初期化(未実装)
                        refresh_delay : "",     //一旦初期化(未実装)
                        refresh_period : "",    //一旦初期化(未実装)
                        history : e.children("history").text(),
                        trends : e.children("trends").text(),
                        deltaStr : "",          //一旦初期化
                        delta : e.children("delta").text(),   //保存時の計算
                        valuemapStr : "",       //一旦初期化
                        valuemap : e.children("valuemap").text(),
                        application : _appName,
                        inventory : "",         //一旦初期化(未実装)
                        description : e.children("description").text(),
                        status : e.children("status").text(),
                        note : ""               //一旦初期化
                    }
                    
                    //obj.typeStrの書き換え
                    obj.typeStr = get_typeStr(obj.type)
                    obj.value_typeStr = get_value_typeStr(obj.value_type)
                    obj.data_typeStr = get_data_typeStr(obj.data_type)
                    obj.deltaStr = get_data_typeStr(obj.delta)
                    obj.valuemapStr = get_valuemapStr(obj.valuemap)
                    
                    //整形したオブジェクトを配列に格納
                    itemSheet.push(obj)
                }
            })
        }
    })
    var insertStr = ""
    itemSheet.forEach(e => {
        insertStr = `<tr>
            <td>${e.templateName}</td>
            <td>${e.itemName}</td>
            <td>${e.typeStr}</td>
            <td>${e.type}</td>
            <td>${e.key}</td>
            <td>${e.snmp_oid}</td>
            <td>${e.params}</td>
            <td>${e.snmp_community}</td>
            <td>${e.port}</td>
            <td>${e.value_typeStr}</td>
            <td>${e.value_type}</td>
            <td>${e.data_typeStr}</td>
            <td>${e.data_type}</td>
            <td>${e.unit}</td>
            <td>${e.formula}</td>
            <td>${e.delay}</td>
            <td>${e.refresh_type}</td>
            <td>${e.refresh_delay}</td>
            <td${e.refresh_period}</td>
            <td>${e.history}</td>
            <td>${e.trends}</td>
            <td>${e.deltaStr}</td>
            <td>${e.delta}</td>
            <td>${e.valuemapStr}</td>
            <td>${e.application}</td>
            <td>${e.inventory}</td>
            <td>${e.description}</td>
            <td>${e.status}</td>
            <td>${e.note}</td>
            </tr>`
        $("#item").append(insertStr)
    })
}

//ApplicationSシートにデータを出力する関数
var _setApplicationSheet = function(xml){
    var applicationSheet = [
        //{
        //  templateName:"",
        //  appName:"",
        //  note:""
        //}
    ]
    var logHeader = "func_setApplicationSheet"
    $(xml).find("template").each(function(){
        if ($(this).children("template") != ""){
            var _templateName = $(this).children("name").text()
            var _appName = ""
            var _note = ""
            $(this).find("application").each(function(){
                _appName = $(this).children("name").text()
                applicationSheet.push({
                    templateName : _templateName,
                    appName : _appName,
                    note : _note
                })
            })
        }
    })
    var insertStr = ""
    applicationSheet.forEach(e => {
        insertStr = `<tr><td>${e.templateName}</td><td>${e.appName}</td><td>${e.note}</td></tr>`
        $("#application").append(insertStr)
    });
}

//templateシートにデータを出力する関数
//2018/04/23 テンプレートとのリンクは未対応
var _setTemplateSheet = function(xml){
    //templateシートのデータ構造
    var templateSheet = [
        //{
        //    templateName:"",
        //    screenName:"",
        //    groups:"",
        //    templateLinks:"",
        //    note:""
        //}
        ]
    var logHeader = "func _setTemplateData:"
    $(xml).find("template").each(function(){
        if ($(this).children("template").text() != ""){
            var _element = $(this)
            var _templateName = $(this).children("template").text()
            var _screenName = $(this).children("name").text()
            var _groups = new String("")    //初期化
            var _templateLinks = new String("")   //初期化

            var _note = "" //コメントはxmlにはないのでデフォルトで空白

            //groupの取得処理
            _element = $(this).children("groups")
            _element.find("group").each(function(){
                if ($(this).children("name").text() == ""){
                    console.log(logHeader + "grpups.group.name is empty.\n")
                }else{
                    _groups += $(this).children("name").text() + ","
                }
            })
            _groups = _groups.replace(_regstr, '')
            
            //templateLinksの取得処理
            _element = $(this).children("tempaltes")
            _element.find("template").each(function(){
                if ($(this).children("name").text() == ""){
                    console.log(logHeader + "templates.template.name is empty.")
                }else{
                    _templateLinks += $(this).children("name").text() + ","
                }
            })
            _templateLinks = _templateLinks.replace(_regstr, '')

            templateSheet.push({
                templateName : _templateName,
                screenName : _screenName,
                groups : _groups,
                templateLinks : _templateLinks,
                note : _note
            })
        }else{
            //console.log(logHeader + "<template> tag has not name.\n")
        }
    })
    //console.log(templateSheet)
    var insertStr = ""
    templateSheet.forEach(e => {
        insertStr = `<tr><td>${e.templateName}</td><td>${e.screenName}</td><td>${e.groups}</td><td>${e.templateLinks}</td><td>${e.note}</td></tr>`
        $("#template").append(insertStr)
    });
}

