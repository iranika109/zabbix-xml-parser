# 概要
このツールはzabbixからエクスポートできるxmlの解析ツールです。

# 使い方
index.htmlと同じ階層にZabbixからエクスポートした以下のxmlファイルを置いてください。  
- zbx_export_templates.xml  #テンプレートのxml  
- zbx_export_hosts.xml      #ホストのxml  

index.htmlをブラウザで開きます。  
うまく動かない場合はあきらめてください。  

# 未実装のパラメーターについて
処理が面倒なので実装を一旦スキップしているパラメーターがあります。
後々実装する予定です(予定はあくまで予定...)
詳細についてはissueを確認してください。

# 動作確認
Zabbix3.0.5
FireFox 